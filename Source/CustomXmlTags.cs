﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Xml;
using HarmonyLib;
using Mohawk.SystemCore;
using TenCrowns.AppCore;
using TenCrowns.GameCore;

namespace CustomXmlTags {
  public class CustomXmlTags : ModEntryPointAdapter {
    public const string MY_HARMONY_ID = "Thremtopod.CustomXmlTags.patch";
    public static Harmony harmony;

    public static readonly Dictionary<Type, SetList<XmlTagData>> InfoTypeToNewTags =
        new Dictionary<Type, SetList<XmlTagData>>();

    public override void Initialize(ModSettings modSettings) {
      if (harmony != null) {
        return;
      }
      harmony = new Harmony(MY_HARMONY_ID);
      harmony.PatchAll();
      Assembly assembly = Assembly.GetAssembly(typeof(InfoBase));
      foreach (Type type in assembly.GetTypes()) {
        if (!type.IsSubclassOf(typeof(InfoBase)) || type.IsAbstract) {
          continue;
        }
        MethodInfo method = type.GetMethod("Read", new Type[] { typeof(Infos), typeof(Infos.ReadContext) });
        if (method != null) {
          harmony.Patch(
              method,
              postfix: new HarmonyMethod(
                  typeof(PatchInfoBase).GetMethod(
                      "PostfixInfoBaseRead", new Type[] { typeof(InfoBase),  typeof(Infos), typeof(Infos.ReadContext) })));
        }
      }
    }

    public override void Shutdown() {
      if (harmony == null) {
        return;
      }
      InfoTypeToNewTags.Clear();
      harmony.UnpatchAll(MY_HARMONY_ID);
      harmony = null;
    }

    public static void AppendTags<TInfo, TData>(Func<TInfo, TData> additionalDataGetter)
        where TInfo : InfoBase where TData : IInfoAdditionalData<TInfo> {
      if (!InfoTypeToNewTags.ContainsKey(typeof(TInfo))) {
        InfoTypeToNewTags[typeof(TInfo)] = new SetList<XmlTagData>();
      }
      foreach (FieldInfo fieldInfo in typeof(TData).GetFields()) {
        XmlTag xmlTag = fieldInfo.GetCustomAttribute<XmlTag>(false);
        if (xmlTag == null) {
          continue;
        }
        XmlTagData data = new XmlTagData(
            xmlTag.Name ?? fieldInfo.Name, xmlTag.Order, xmlTag.ShouldReadData, fieldInfo,
            o => additionalDataGetter.Invoke((TInfo) o));
        if (!InfoTypeToNewTags[typeof(TInfo)].Contains(data)) {
          InfoTypeToNewTags[typeof(TInfo)].Add(data);
        }
      }
    }

    public static void AppendGlobals<TData>(Func<InfoGlobals, TData> additionalDataGetter)
        where TData : IInfoAdditionalData<InfoGlobals> {
      if (!InfoTypeToNewTags.ContainsKey(typeof(InfoGlobals))) {
        InfoTypeToNewTags[typeof(InfoGlobals)] = new SetList<XmlTagData>();
      }
      foreach (FieldInfo fieldInfo in typeof(TData).GetFields()) {
        XmlTag xmlTag = fieldInfo.GetCustomAttribute<XmlTag>(false);
        if (xmlTag == null) {
          continue;
        }
        XmlTagData data = new XmlTagData(
            xmlTag.Name ?? fieldInfo.Name, xmlTag.Order, xmlTag.ShouldReadData, fieldInfo,
            o => additionalDataGetter.Invoke((InfoGlobals) o));
        if (!InfoTypeToNewTags[typeof(InfoGlobals)].Contains(data)) {
          InfoTypeToNewTags[typeof(InfoGlobals)].Add(data);
        }
      }
    }
  }

  public struct XmlTagData : IComparable<XmlTagData> {
    public readonly string Name;
    public readonly int Order;
    public readonly bool ShouldReadData;
    public readonly FieldInfo FieldInfo;
    public readonly Func<object, object> AdditionalDataGetter;

    public XmlTagData(
        string name, int order, bool shouldReadData, FieldInfo fieldInfo, Func<object, object> additionalDataGetter
    ) {
      Name = name;
      Order = order;
      ShouldReadData = shouldReadData;
      FieldInfo = fieldInfo;
      AdditionalDataGetter = additionalDataGetter;
    }

    public int CompareTo(XmlTagData other) {
      return Name.Equals(other.Name) ? 0 : Order.CompareTo(other.Order);
    }

    public override bool Equals(object obj) {
      return obj is XmlTagData other && Name.Equals(other.Name);
    }

    public override int GetHashCode() {
      return Name.GetHashCode();
    }
  }
}