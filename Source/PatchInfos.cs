using System;
using System.Collections.Generic;
using System.Reflection;
using System.Xml;
using HarmonyLib;
using Mohawk.SystemCore;
using TenCrowns.GameCore;

namespace CustomXmlTags {
  [HarmonyPatch(typeof(Infos))]
  public class PatchInfos {
    [HarmonyPostfix]
    [HarmonyPatch(typeof(Infos), "getModdableBaseXML", typeof(string))]
    static void PostfixGetModdableBaseXML(Infos __instance, string zFilename, ref List<XmlDocument> __result) {
      List<XmlDataListItemBase> mInfoList =
          new Traverse(__instance).Field<List<XmlDataListItemBase>>("mInfoList").Value;
      Type infoType = null;
      foreach (XmlDataListItemBase item in mInfoList) {
        if (item.GetFileName() != zFilename) {
          continue;
        }
        Type[] genericTypeArguments = item.GetType().GenericTypeArguments;
        if (genericTypeArguments.Length > 0 && CustomXmlTags.InfoTypeToNewTags.ContainsKey(genericTypeArguments[0])) {
          infoType = genericTypeArguments[0];
          break;
        }
      }

      // No tags added for this file
      if (infoType == null) {
        return;
      }

      SetList<XmlTagData> newTags = CustomXmlTags.InfoTypeToNewTags[infoType];
      List<XmlDocument> temp = new List<XmlDocument>();
      foreach (XmlDocument xmlDocument in __result) {
        XmlDocument clone = (XmlDocument) xmlDocument.Clone();
        temp.Add(clone);
      }
      // Only mutate cloned XmlDocuments, because mutations to XmlDocuments are persisted after mod is unloaded!
      __result = temp;

      foreach (XmlDocument xmlDocument in __result) {
        // Find first Root/Entry node
        XmlNodeList nodes = xmlDocument.SelectNodes("Root/Entry");
        if (nodes == null || nodes.Count == 0) {
          continue;
        }
        XmlNode node = nodes[0];
        if (node.ChildNodes.Count == 0) {
          continue;
        }

        XmlElement childElement = null;
        for (var index = node.ChildNodes.Count - 1; index >= 0; index--) {
          object childNode = node.ChildNodes[index];
          if (childNode is XmlElement element) {
            // Only add tags that are empty and have not been added yet
            if (element.IsEmpty && newTags[newTags.Count - 1].Name != element.Name) {
              childElement = element;
            }
            break;
          }
        }
        if (childElement?.OwnerDocument == null) {
          continue;
        }

        foreach (XmlTagData newTag in newTags) {
          node.AppendChild(
              childElement.OwnerDocument.CreateElement(childElement.Prefix, newTag.Name, childElement.NamespaceURI));
        }
      }
    }
  }

  public class PatchInfoBase {
    public static void PostfixInfoBaseRead(InfoBase __instance, Infos infos, Infos.ReadContext ctx) {
      if (!CustomXmlTags.InfoTypeToNewTags.ContainsKey(__instance.GetType())) {
        return;
      }

      foreach (XmlTagData xmlTagData in CustomXmlTags.InfoTypeToNewTags[__instance.GetType()]) {
        if (!xmlTagData.ShouldReadData) {
          continue;
        }
        object additionalData = xmlTagData.AdditionalDataGetter.Invoke(__instance);
        Type type = xmlTagData.FieldInfo.FieldType;
        if (type == typeof(int)) {
          int value = (int) xmlTagData.FieldInfo.GetValue(additionalData);
          infos.readInt(ctx, xmlTagData.Name, ref value);
          xmlTagData.FieldInfo.SetValue(additionalData, value);
        } else if (type == typeof(float)) {
          float value = (float) xmlTagData.FieldInfo.GetValue(additionalData);
          infos.readFloat(ctx, xmlTagData.Name, ref value);
          xmlTagData.FieldInfo.SetValue(additionalData, value);
        } else if (type == typeof(bool)) {
          bool value = (bool) xmlTagData.FieldInfo.GetValue(additionalData);
          infos.readBool(ctx, xmlTagData.Name, ref value);
          xmlTagData.FieldInfo.SetValue(additionalData, value);
        } else if (type == typeof(string)) {
          string value = (string) xmlTagData.FieldInfo.GetValue(additionalData);
          infos.readString(ctx, xmlTagData.Name, ref value);
          xmlTagData.FieldInfo.SetValue(additionalData, value);
        } else {
          MohawkAssert.Assert(false);
        }
      }
    }
  }

  [HarmonyPatch(typeof(InfoGlobals))]
  public class PatchInfoGlobals {
    [HarmonyPostfix]
    [HarmonyPatch(typeof(InfoGlobals), "ReadData", typeof(Infos))]
    static void PostfixInfoGlobalsReadData(InfoGlobals __instance, Infos infos) {
      if (!CustomXmlTags.InfoTypeToNewTags.ContainsKey(__instance.GetType())) {
        return;
      }

      foreach (XmlTagData xmlTagData in CustomXmlTags.InfoTypeToNewTags[__instance.GetType()]) {
        if (!xmlTagData.ShouldReadData) {
          continue;
        }
        Type type = xmlTagData.FieldInfo.FieldType;
        if (type == typeof(int)) {
          int value = infos.getGlobalInt(xmlTagData.Name);
          xmlTagData.FieldInfo.SetValue(xmlTagData.AdditionalDataGetter.Invoke(__instance), value);
        } else if (type == typeof(float) || type == typeof(double)) {
          float value = infos.getGlobalFloat(xmlTagData.Name);
          xmlTagData.FieldInfo.SetValue(xmlTagData.AdditionalDataGetter.Invoke(__instance), value);
        } else if (type == typeof(string)) {
          string value = infos.getGlobalString(xmlTagData.Name);
          xmlTagData.FieldInfo.SetValue(xmlTagData.AdditionalDataGetter.Invoke(__instance), value);
        } else {
          MethodInfo getGlobalType = typeof(Infos).GetMethod("getGlobalType", new Type[] { typeof(string) })
              .MakeGenericMethod(new Type[] { xmlTagData.FieldInfo.FieldType });
          object value = getGlobalType.Invoke(infos, new object[] { xmlTagData.Name });
          xmlTagData.FieldInfo.SetValue(xmlTagData.AdditionalDataGetter.Invoke(__instance), value);
        }
      }
    }
  }
}