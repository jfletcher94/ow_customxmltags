using System;
using System.Runtime.CompilerServices;

namespace CustomXmlTags {
  [AttributeUsage(AttributeTargets.Field)]
  public class XmlTag : Attribute {
    public readonly string Name;
    public readonly bool ShouldReadData;
    public readonly int Order;

    public XmlTag(string name = null, bool shouldReadData = true, [CallerLineNumber] int order = 0) {
      Name = name;
      ShouldReadData = shouldReadData;
      Order = order;
    }
  }
}